import { Controller, Get } from '@nestjs/common';

@Controller('version')
export class VersionController {
  @Get()
  async getVersion(): Promise<{ version: string }> {
    return { version: '0.1' };
  }
}
