import { it, expect } from 'vitest';
import { mount } from '@vue/test-utils';
import Index from './index.vue';

it('should display the nuxt welcome page', () => {
  const nuxtWelcomeSelector = '[data-testid=nuxt-welcome]';

  const wrapper = mount(Index);

  const nuxtWelcome = wrapper.get(nuxtWelcomeSelector);

  expect(nuxtWelcome).toBeDefined();
});
