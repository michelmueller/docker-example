import { it, describe, expect } from 'vitest';
import Version from './version.vue';
import { appVersion } from '~/lib/version';
import { mountSuspended, registerEndpoint } from '@nuxt/test-utils/runtime';

registerEndpoint('/api/version', {
  method: 'GET',
  handler: () => ({ version: '1.2' }),
});

describe('version page', () => {
  it('should display the frontend version on the version page', async () => {
    const frontendVersionSelector = '[data-testid=frontend-version]';
    const wrapper = await mountSuspended(Version);
    const frontendVersionNode = wrapper.get(frontendVersionSelector);

    expect(frontendVersionNode.text()).toEqual(appVersion);
  });

  it('should display the backend version on the version page', async () => {
    const backendVersionSelector = '[data-testid=backend-version]';
    const wrapper = await mountSuspended(Version);
    const backendVersionNode = wrapper.get(backendVersionSelector);

    expect(backendVersionNode.text()).toEqual('1.2');
  });
});
