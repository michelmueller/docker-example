// https://nuxt.com/docs/api/configuration/nuxt-config

const apiURL = import.meta.env.API_URL;
if (!apiURL) {
  console.error('No API URL provided');
}

export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ['@nuxt/test-utils/module'],
  routeRules: {
    '/api/**': { proxy: `${import.meta.env.API_URL}/**` },
  },
});
